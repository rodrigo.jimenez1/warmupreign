# WarmUpReign

Proyecto de Warm-up de Rodrigo Jiménez.

## Getting started

Run the docker compose file in the root of the project.

```
docker-compose up --build
```

To refresh the database, visit the link to force-update with the latest hits from the API.

```
http://<docker-container-ip>:3000/news-feed/update
```

Every time a hit is duplicated, it will be marked as 'error' and will not be integrated in the db.

To visit the news-feed you can use


```
http://<docker-container-ip>:8080/
```
