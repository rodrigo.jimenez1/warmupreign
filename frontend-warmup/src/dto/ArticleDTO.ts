export interface ArticleDTO {
    readonly _id : string;
    readonly __v : number;
    readonly story_id : number;
    readonly display_title : string;
    readonly author : string;
    readonly creation_date : string;
    readonly url : string;
    readonly deleted : boolean;
}