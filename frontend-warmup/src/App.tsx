import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppBar from './components/AppBar/AppBar';
import NewsFeed from './components/NewsFeed/NewsFeed';

function App() {
  return (
    <div className="App">
      <AppBar />
      <NewsFeed />
    </div>
  );
}

export default App;
