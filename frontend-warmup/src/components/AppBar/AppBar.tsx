import React, { FC } from 'react';
import './AppBar.css';

interface AppBarProps {}

const AppBar: FC<AppBarProps> = () => (
  <div className="AppBar" data-testid="AppBar">
    <h1>HN Feed</h1>
    <h3>We &lt;3 hacker news!</h3>
  </div>
);

export default AppBar;
