import { Component, FC, MouseEventHandler, useState } from 'react';
import { ArticleDTO } from '../../dto/ArticleDTO';
import './ArticleRow.css';
import 'material-icons/iconfont/material-icons.css';

interface ArticleRowProps {

  content : ArticleDTO,
  deleteArticle: any

}


class ArticleRow extends Component<ArticleRowProps,{isVisible : boolean}> {

  constructor(props:ArticleRowProps){
    super(props);
    this.state = {isVisible:false};
    this.deleteArticle = this.deleteArticle.bind(this);
  }

  ParseDate(dateToParse : string){
        const ParsedDate = new Date(dateToParse);
        const today = new Date().getDay();
      
        
        if (ParsedDate.getDay() == today)
          return "today";
        if (ParsedDate.getDay() == today-1)
          return "yesterday";
      
        var options : Intl.DateTimeFormatOptions = { month: 'short', day: 'numeric' };
        return new Intl.DateTimeFormat('en-US', options).format(ParsedDate);
      }

    showIcon(newState : boolean){
      this.setState({isVisible:newState});
    }

    deleteArticle(event : any){
        event.stopPropagation();
        this.props.deleteArticle(this.props.content._id);
    }

  render() {
      return (
        <div className="ArticleRow" data-testid="ArticleRow" onMouseEnter={() => this.showIcon(true)} onMouseLeave={() => this.showIcon(false)} onMouseUp={() => window.open(this.props.content.url)} >
            <div >
              <label className="title">{this.props.content.display_title}</label>
              <label className="author">- {this.props.content.author} -</label>
              <label className='icon' style={{visibility: this.state.isVisible ? "visible" : "hidden"}}> <span className="material-icons" onMouseUp={this.deleteArticle}>delete</span></label>  
              <label className="date">{this.ParseDate(this.props.content.creation_date)}</label>
            </div>

        </div> 
      );
  }

}

export default ArticleRow