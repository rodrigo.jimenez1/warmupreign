import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ArticleRow from './ArticleRow';
import { ArticleDTO } from '../../dto/ArticleDTO';

describe('<ArticleRow />', () => {
  test('it should mount', () => {
    const fakeArticle : ArticleDTO = {
      _id : "0",
      __v : 0,
      story_id : 0,
      display_title : "Fake article",
      author : "El compare",
      creation_date : ('1995-12-17T03:24:00'),
      url : "0",
      deleted : false,
    }
    render(<ArticleRow content={fakeArticle} deleteArticle={null}/> );
    
    const articleRow = screen.getByTestId('ArticleRow');

    expect(articleRow).toBeInTheDocument();
  });
});