import React, { FC, useEffect, useState } from 'react';
import './NewsFeed.css';
import axios from "axios"
import ArticleRow from '../ArticleRow/ArticleRow';
import { ArticleDTO } from '../../dto/ArticleDTO';
import { unmountComponentAtNode } from 'react-dom';

interface NewsFeedProps {}



async function fetchFromApi(url:string) {
  var resp = await axios.get(url);
  return resp.data;
}


const NewsFeed: FC<NewsFeedProps> = () => { 

  const [articles,SetArticles] = useState<ArticleDTO[]>([]);
  const [toDelete,SetDelete] = useState<string[]>([]);
  const [fetched,SetFecthState] = useState(false);

  useEffect(() => {
    async function fetchFromApi(url:string) {
      var resp = await axios.get(url);
      SetArticles(resp.data);
      SetFecthState(true);
      return resp.data;
    }
    if(!fetched)
      fetchFromApi("http://localhost:3000/news-feed/show");    
  })

  useEffect(() => {
    async function DeleteFromDB(){
      var resp = await axios.post("http://localhost:3000/news-feed/deleteids",{toDelete});
      SetDelete([]);
      console.log("lol");
      
      return resp.data;
    }
    if (toDelete.length != 0)
      DeleteFromDB();
    
  }, [toDelete])


  function deleteArticle(rowToDelete : string) {
    console.log(rowToDelete);
    var newArt = Array.from(articles);
    var newDel = Array.from(toDelete);
    newDel.push(rowToDelete);
    SetDelete(newDel)
    newArt = newArt.filter(row => row._id!==rowToDelete);
    SetArticles(newArt);
    
  }

  return (
      <div className="NewsFeed" data-testid="NewsFeed">
        { articles.map( article => (
          <ArticleRow key={article.story_id} content={article} deleteArticle={deleteArticle}/>
        ) ) }
      </div>
    )}
;

export default NewsFeed;
