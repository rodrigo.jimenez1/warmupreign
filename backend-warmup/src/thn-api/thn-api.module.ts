import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ThnApiService } from './thn-api.service';

@Module({
    imports: [HttpModule],
    providers: [ThnApiService],
    exports : [ThnApiService]
})
export class ThnApiModule {}
