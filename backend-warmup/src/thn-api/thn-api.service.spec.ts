import { HttpModule, HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ThnApiService } from './thn-api.service';

describe('ThnApiService', () => {
  let service: ThnApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ThnApiService],
      imports: [HttpModule],
    }).compile();

    service = module.get<ThnApiService>(ThnApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  test('should return a clean dictionary of articles', () => {
    return service.FetchNews().then((data) => {
      const hits = Object.keys(data).length;
      expect(hits).toBeGreaterThan(0);
      console.log("Hits from API: " + hits);
      
    });
  });
  

});
