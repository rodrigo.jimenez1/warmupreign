import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom, lastValueFrom, map } from 'rxjs';
import { CreateArticleDTO } from 'src/news-feed/dto/article.dto';
import { Interval } from '@nestjs/schedule';

@Injectable()
export class ThnApiService {

    constructor(private httpService : HttpService){}

    @Interval(3600000)
    async FetchNews(){
        const url = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
        const data  = await lastValueFrom(this.httpService.get(url))
                            .then(val => {return val.data.hits});
        // console.log(data);
        
        var parsedData = {};

        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            const id = element.story_id;
            if (id == null){
                continue;
            } else {
                if (id in parsedData) continue;
            }
            var title = "";
            if (element.story_title != null){
                title = element.story_title;
            } else {
                if (element.title != null){
                    title = element.title;
                } else {
                    continue;
                }
            }

            var storyUrl = "";
            if (element.story_url != null){
                storyUrl = element.story_url;
            } else {
                if (element.url != null){
                    storyUrl = element.title;
                } else {
                    continue;
                }
            }

            var author = (element.author != null) ? element.author : "Unknown";
            var creationDate = element.created_at;

            const articleData:CreateArticleDTO = { 
                story_id : id,
                display_title : title,
                author : author,
                creation_date : creationDate,
                url : storyUrl,
                deleted : false
            }

            parsedData[articleData.story_id] = articleData;
            // console.log(articleData.story_id);
            
            
        }

        return parsedData;
    }



}
