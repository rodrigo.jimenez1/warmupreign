import { Schema } from "mongoose";

export const ArticleSchema = new Schema({
    story_id: Number,
    display_title: String,
    author: String,
    creation_date: Date,
    url: String,
    deleted: Boolean
});