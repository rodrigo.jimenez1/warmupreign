import { Module } from '@nestjs/common';
import { NewsFeedService } from './news-feed.service';
import { NewsFeedController } from './news-feed.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from './schemas/article.schema';
import { ThnApiService } from 'src/thn-api/thn-api.service';
import { ThnApiModule } from 'src/thn-api/thn-api.module';



@Module({
  imports: [ 
  MongooseModule.forFeature([
    {name: 'Article', schema: ArticleSchema}
  ]),
  ThnApiModule
],
  providers: [NewsFeedService],
  controllers: [NewsFeedController]
})
export class NewsFeedModule {}
