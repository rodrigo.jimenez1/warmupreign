import { ConflictException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateArticleDTO } from './dto/article.dto';
import { Article } from './article.interface';
import { create } from 'domain';

@Injectable()
export class NewsFeedService {

    constructor(@InjectModel('Article') private readonly articleModel : Model<Article>) {}

    async getArticles(): Promise<Article[]> {
        const articles = await this.articleModel.find( {deleted:false} ).sort({creation_date:-1});
        return articles;
    }

    
    async createArticle(createArticleDTO : CreateArticleDTO) : Promise<Partial<Article>>{
        
        // console.log(createArticleDTO.title);
        
        const exists = await this.doesArticleExists(createArticleDTO);
        
        if(exists){            
            throw new ConflictException('Article with the same ID already exists.');
        }
        const article = await this.articleModel.create(createArticleDTO as any);
        return {
            story_id: article.story_id,
            display_title: article.display_title,
            author: article.author,
            creation_date: article.creation_date,
            url: article.url,
            deleted: article.deleted,
        };
    }
    
    
    async deleteOne(id : string){
        return this.articleModel.updateOne({ "_id" : id },{$set: {deleted: true}}).exec();
    }
    
    async doesArticleExists(createArticleDTO: CreateArticleDTO) {
        
        // console.log(createArticleDTO.story_id);
        var findQuery = await this.articleModel.findOne({ story_id: createArticleDTO.story_id }).exec();
        if(findQuery != null){
            return true;
        }
        return false;
        
    }
    
    
    //Test methods
    async getArticle(articleID : string) : Promise<Article>{
        const article = await this.articleModel.findOne({story_id: articleID}).exec();
        return article;
    }
    async dropCollection(){
        return this.articleModel.deleteMany({});
    }
    
    async resetAllDeletes(){
        return this.articleModel.updateMany({},{$set: {deleted: false}})
    }
}
