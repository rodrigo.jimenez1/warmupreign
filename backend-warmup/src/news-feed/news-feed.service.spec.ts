import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model, Query } from 'mongoose';
import { Article } from './article.interface';
import { CreateArticleDTO } from './dto/article.dto';
import { NewsFeedService } from './news-feed.service';
import { ArticleSchema } from './schemas/article.schema';
import { createMock } from '@golevelup/ts-jest';

const today = new Date();

const mockArticle = (
  deleted = false,
  story_id = 0,
  display_title = "Fake article",
  author = "El compare",
  creation_date = today,
  url = "0",
): CreateArticleDTO => ({
  deleted,
  story_id,
  display_title,
  author,
  creation_date,
  url,
});

const mockArticleDoc = (mock?: Partial<CreateArticleDTO>): Partial<Article> => (
  {
    story_id : mock?.story_id || 0,
    display_title : mock?.display_title || "Fake article",
    author : mock?.author || "El compare",
    creation_date : mock?.creation_date || today,
    url : mock?.url || "0",
    deleted : mock?.deleted || false,
  }
)

const articleDocArray = [
  mockArticleDoc(),
  mockArticleDoc(),
  mockArticleDoc(),
  mockArticleDoc(),

]

describe('NewsFeedService', () => {
  let service: NewsFeedService;

  let model: Model<Article>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsFeedService, {
        provide: getModelToken('Article'),
        useValue: {
          find : jest.fn(), //insert mock dto
          findOne : jest.fn(),
          findByID : jest.fn(),
          constructor : jest.fn().mockResolvedValue(mockArticle()),
          create : jest.fn(),
          deleteMany : jest.fn(),
          updateOne : jest.fn(),
          updateMany : jest.fn(),
        }
      }],
    }).compile();

    service = module.get<NewsFeedService>(NewsFeedService);
    model = module.get<Model<Article>>(getModelToken('Article'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });


  it('should return all articles from newest to oldest', async () => {
    jest.spyOn(model,'find').mockReturnValue({
      sort: jest.fn().mockResolvedValueOnce(articleDocArray),
    } as any);
    const articles = await service.getArticles();
    expect(articles).toEqual(articleDocArray);
  })


  it('should return an article based on its ID', async () => {
    const mockArticleToGet = mockArticleDoc();
    jest.spyOn(model,'findOne').mockReturnValueOnce(
      createMock<Query<Article,Article>>({
        exec: jest
        .fn()
        .mockResolvedValueOnce(
          mockArticleDoc(),
        ),
      }) as any,
    );

    const foundArticle = await service.getArticle(mockArticleToGet.id);
    expect(foundArticle).toEqual(mockArticleToGet);

  })


  it('should create a new article', async () => {
    const newArticleDTO = mockArticle();

    jest.spyOn(service,'doesArticleExists').mockImplementationOnce(() => 
      Promise.resolve(false)
    );

    jest.spyOn(model,'create').mockImplementationOnce(() =>
      Promise.resolve(mockArticle())
    );
    

    let newArticle;
    try {
      newArticle = await service.createArticle(newArticleDTO);
    } catch (error) {
      console.log("error" + error.message);
      newArticle = "error while testing";
    }
    expect(newArticle).toEqual(mockArticleDoc(mockArticle()));
    
  });

  it('should check the article existence in the db', async () => {
    const mockArticleToGet = mockArticle();
    jest.spyOn(model,'findOne').mockReturnValueOnce(
      createMock<Query<Article,Article>>({
        exec: jest
        .fn()
        .mockResolvedValueOnce(
          mockArticleDoc(),
        ),
      }) as any,
    );
    const existenceResponse = await service.doesArticleExists(mockArticleToGet);

    expect(existenceResponse).toEqual(true);

  });


  it('should delete one article selected by id', async () => {

    var deletedArticleMock = mockArticle(true);
    jest.spyOn(model,"updateOne").mockReturnValueOnce(
      createMock<Query<Article,Article>>({
        exec: jest.fn().mockResolvedValueOnce({
          deletedArticleMock
        }),
      }) as any
    )

    const deletedArticle = await service.deleteOne(`${mockArticle().story_id}`)

  });

});
