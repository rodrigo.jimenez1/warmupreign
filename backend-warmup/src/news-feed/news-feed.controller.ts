import { Controller, Post, Res, HttpStatus, Get, Body } from '@nestjs/common';
import { firstValueFrom, lastValueFrom, map } from 'rxjs';
import { NewsFeedService } from './news-feed.service';
import { CreateArticleDTO } from './dto/article.dto';
import { ThnApiService } from '../thn-api/thn-api.service';

@Controller('news-feed')
export class NewsFeedController {


    constructor(private newsFeedService : NewsFeedService,private thnApiService : ThnApiService) {}

    @Get('/show')
    async ShowNews(@Res() res, @Body() body){

        const foundArticles = await this.newsFeedService.getArticles();

        return res.status(HttpStatus.OK).send(foundArticles);
    }

    @Get('/update')
    async UpdateNews(@Res() res){
        const fetchedData = await this.thnApiService.FetchNews();

        var createdData = []
        var errors = 0;
        for (const key in fetchedData) {
            try {
                const element = await this.newsFeedService.createArticle(fetchedData[key]);
                createdData.push(element);
            } catch (exception) {
                console.log(" - " + exception.message);
                errors++;
                continue;
            }
        }

        res.status(HttpStatus.OK).json({
            createdData,
            errors
        });
    }


    @Get("/drop-all")
    async DropAllArticles(@Res() res){
        var response = await this.newsFeedService.dropCollection();
        
        res.status(HttpStatus.OK).json({
            response
        });
    }

    @Post("/rollback")
    async DoRollback(@Res() res){
        var response = await this.newsFeedService.resetAllDeletes();
        res.status(HttpStatus.OK).json({
            response
        });
    }

    @Post("/deleteids")
    async DeleteBulkIds(@Res() res,@Body() body){

        var deletedCount = 0;
        for (let index = 0; index < body.toDelete.length; index++) {
            const element = body.toDelete[index];
            try {
                var response = await this.newsFeedService.deleteOne(element);            
                console.log(response);
            } catch (error) {
                console.log("Error");  
                continue;                
            }
            deletedCount++;
        }
        
        res.status(HttpStatus.OK).json({
            deletedCount
        });
    }
}
