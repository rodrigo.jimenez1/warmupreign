export class CreateArticleDTO{
    readonly story_id : number;
    readonly display_title : string;
    readonly author : string;
    readonly creation_date : Date;
    readonly url : string;
    readonly deleted : boolean;
}
