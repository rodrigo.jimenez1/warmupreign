import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsFeedModule } from './news-feed/news-feed.module';
import { ThnApiService } from './thn-api/thn-api.service';
import { ThnApiModule } from './thn-api/thn-api.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://db:27017/news-feed-data'),
    NewsFeedModule,
    ThnApiModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
